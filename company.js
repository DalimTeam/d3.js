(function(d3) {	
	var company = {
		svg: null,
		departmentRadius: 200,
		
		companyDotRadius: 24,
		departmentsDotRadius: 16,
		companyFontSize: 16,
		departmentFontSize: 16,
		fontPadding: 10,
		userDotRadius: 8,
		teamWidth: 150,
		
		centerX: 0,
		centerY: 0,
		docWidth: 0,
		docHeight: 0,
		zoomed: false,
		
		data: [], 
		InitWindow: function() {
			var w = window,
		    	d = document,
		    	e = d.documentElement,
		    	g = d.getElementsByTagName('body')[0],
		    	x = w.innerWidth || e.clientWidth || g.clientWidth,
		    	y = w.innerHeight|| e.clientHeight|| g.clientHeight;
			this.docWidth = x; 
			this.docHeight = y;
			this.centerX = this.docWidth / 2;
			this.centerY = this.docHeight / 2;
			this.svg = d3.select("body")
				.append("div")
				.classed("svg-container", true)
				.append("svg")
				.classed("svg-content", true)
				.attr({
					preserveAspectRatio: "xMinYMin meet",
					viewBox: "0 0 " + this.docWidth + " " + this.docHeight
				});
			this.svg.append("g")
					.attr("id", "fullgroup");
			this.departmentRadius = Math.min(this.centerX, this.centerY) - 50;
		},
		
		DrawUserInfo: function(user, dp, ch) {
			var offsetY = ch[1] > 85 ? ch[1] - 85 : ch[1] + 5, offsetX, objWidth;
			var obj = d3.select("body")
				.append("div")
				.classed("user-container", true);
			obj.append("p")
				.text("Name: " + user.name);
			obj.append("p")
				.text("Department: " + dp);  
			obj.append("p")
				.text("Phone: " + user.phone);
			
			objWidth = obj.node().getBoundingClientRect().width;
			offsetX = ch[0] > this.docWidth - objWidth - 5 ? ch[0] - objWidth - 5 : ch[0] + 5; 
			obj.style({
				left: offsetX + "px",
				top: offsetY + "px"
			});
		},
		
		RemoveUserInfo: function() {
			d3.select("body")
				.select(".user-container")
				.remove(); 
		},
		
		DrawCompany: function() {
			var parent = this;
			this.data = [];
			d3.json("json.php", function(error, root) {
				parent.data.push({
					title: root.title,
					size: parent.companyDotRadius
				});
				parent.data = parent.data.concat(root.departments);
				for (var i = 1, l = parent.data.length; i < l; i++) {
					parent.data[i].size = parent.departmentsDotRadius;
				}
				parent.Redraw([parent.data[0]]); 
			});
		},
		
		Redraw: function(data) {
			var parent = this;
			
			var circle = this.svg.select("g")
				.selectAll("circle")
				.data(data, function(d) {return d.title});
			circle.enter().append("line")
				.classed("department-line", true)
				.attr({
					x1: this.centerX,
					y1: this.centerY,
					x2: this.centerX,
					y2: this.centerY, 
					opacity: 0.3
				})
				.style("stroke", "steelblue")
				.transition()
				.duration(750)
				.delay(function(d, i) {return i * 100})
				.attr({
					x2: function(d, i) {return i == 0 ? parent.centerX : Math.cos(Math.PI * 2 / (data.length - 1) * i) * parent.departmentRadius + parent.centerX},
					y2: function(d, i) {return i == 0 ? parent.centerY : Math.sin(Math.PI * 2 / (data.length - 1) * i) * parent.departmentRadius + parent.centerY}
				});

			circle.enter().append("circle")
				.classed("department-circle", true)
				.attr({
					cy: function(d, i) {return i == 0 ? parent.centerY : Math.sin(Math.PI * 2 / (data.length - 1) * i) * parent.departmentRadius + parent.centerY},
					cx: function(d, i) {return i == 0 ? parent.centerX : Math.cos(Math.PI * 2 / (data.length - 1) * i) * parent.departmentRadius + parent.centerX},
					r: 0,
					opacity: 1
				})
				.style({
					fill: "steelblue",
					cursor: "pointer",
				})
				.transition()
				.duration(750)
				.delay(function(d, i) {return i * 100})
				.attr("r", function(d) {return d.size});
				
			circle.enter().append("text")
				.classed("department-text", true)
				.attr({
					x: function(d, i) {return i == 0 ? parent.centerX : Math.cos(Math.PI * 2 / (data.length - 1) * i) * parent.departmentRadius + parent.centerX},
					y: function(d, i) {
						var ungle = Math.PI * 2 / (data.length - 1) * i;
						var offsetY = ungle > 0 && ungle < Math.PI ? parent.departmentsDotRadius + parent.fontPadding * 2 : -(parent.departmentsDotRadius + parent.fontPadding);
						return i == 0 ? parent.centerY - parent.companyDotRadius - parent.fontPadding : Math.sin(Math.PI * 2 / (data.length - 1) * i) * parent.departmentRadius + parent.centerY + offsetY;
					}
				})
				.text(function (d) {return d.title})
				.style({
					"font-family": "Tahoma",
					"font-size": this.departmentFontSize + "px"
				})
				.attr({
					"text-anchor": "middle",
					opacity: 0
				})
				.transition()
				.duration(750)
				.delay(function(d, i) {return i * 100})
				.attr("opacity", 1)
				
			circle.exit().remove();

			this.svg.selectAll("circle")
				.on("click", function(d, i) {
					d3.select(this)
						.attr("r", function(d) {return d.size});
					if (i == 0)
						parent.Redraw(parent.data);
					else
						parent.Zoom(undefined, i);
				})
				.on("mouseover", function(d) {
					d3.select(this)
						.transition()
						.duration(250)
						.attr("r", function(d) {return d.size+3});
					})
				.on("mouseout", function(d) {
					d3.select(this)
						.transition()
						.duration(750)
						.attr("r", function(d) {return d.size});
				});
		},
		
		Zoom: function(obj, index) {
			var pointScale = 5, data = this.data[index].team, parent = this, team, teamWidth = 50, points = [], radiusModifier = 4;

			if (!this.zoomed) {
				d3.selectAll(".department-line")
					.attr("opacity", 0);
				d3.selectAll(".department-circle")
					.attr("opacity", function(d, i) {return i == index ? 1 : 0})
					.attr("cx", function(d, i) { 
						return i == index ? parent.centerX : i == 0 ? 0 : d3.select(this).attr("cx")|0;
					})
					.attr("cy", function(d, i) {
						return i == index ? parent.centerY : i == 0 ? 0 : d3.select(this).attr("cy")|0;
					});
				d3.selectAll(".department-text")
					.transition()
					.duration(300)
					.delay(function(d, i) {return i * 20})
					.attr({
						opacity: function(d, i) {return i == index ? 1 : 0},
						x: function(d, i) { 
							return i == index ? parent.centerX : d3.select(this).attr("x")|0;
						},
						y: function(d, i) {
							return i == index ? parent.centerY - parent.departmentsDotRadius - parent.fontPadding : d3.select(this).attr("y")|0;
						}
					});

				team = this.svg.select("g")
					.selectAll(".team").data(data, function(d) {return d.phone});
				MakeRandomPoint(radiusModifier);
				
				team.enter()
					.append("circle")
					.classed("team", true)
					.attr("cy", function(d, i) {
						return points[i][1];
					})
					.attr("cx", function (d, i) {
						return points[i][0];
					})
					.style("fill", "steelblue")
					.style("cursor", "pointer")
					.attr("r", 0)
					.transition()
					.duration(750)
					.delay(function(d, i) {return i * 30})
					.attr("r", this.userDotRadius);

				this.svg.selectAll(".team")
					.on("mouseover", function(d, i) {
						var obj = d3.select(this);
						parent.DrawUserInfo(data[i], parent.data[index].title, [obj.attr("cx")|0, obj.attr("cy")|0]);
					})
					.on("mouseout", function(d, i) {
						parent.RemoveUserInfo();
					});
			}
			else {
				team = this.svg.select("g")
					.selectAll(".team")
					.data([]);
				team.exit()
					.transition()
					.duration(300)
					.delay(function(d, i) {return i * 20})
					.attr("r", 0)
					.remove();
				this.svg.selectAll(".team-text")
					.transition()
					.duration(300)
					.delay(function(d, i) {return i * 20})
					.attr("opacity", 0)
					.remove();
					
				d3.selectAll(".department-line") 
					.transition() 
					.duration(300)
					.delay(function(d, i) {return i * 20 + data.length * 22})
					.attr("opacity", 0.3);
					
					
					
				setTimeout(function() {
					d3.selectAll(".department-circle")
					.attr({
						cy: function(d, i) {return i == index ? Math.sin(Math.PI * 2 / (parent.data.length - 1) * index) * parent.departmentRadius + parent.centerY : i == 0 ? parent.centerY : d3.select(this).attr("cy")|0},
						cx: function (d, i) {return i == index ? Math.cos(Math.PI * 2 / (parent.data.length - 1) * index) * parent.departmentRadius + parent.centerX : i == 0 ? parent.centerX : d3.select(this).attr("cx")|0},
						opacity: 1
					});
					d3.selectAll(".user-container").remove();
				}, data.length * 25);	
					
				d3.selectAll(".department-text")
					.transition()
					.duration(300)
					.delay(function(d, i) {return i * 20 + data.length * 22})
					.attr("opacity", 1)
					.attr("x", function(d, i) { 
						return i == index ? Math.cos(Math.PI * 2 / (parent.data.length - 1) * index) * parent.departmentRadius + parent.centerX : d3.select(this).attr("x")|0;
					})
					.attr("y", function(d, i) {
						var ungle = Math.PI * 2 / (parent.data.length - 1) * i;
						var offsetY = ungle > 0 && ungle < Math.PI ? parent.departmentsDotRadius + parent.fontPadding * 2 : -(parent.departmentsDotRadius + parent.fontPadding);
						return i == index ? Math.sin(Math.PI * 2 / (parent.data.length - 1) * index) * parent.departmentRadius + parent.centerY + offsetY : d3.select(this).attr("y")|0;
					});
			}
			
			function MakeRandomPoint(radiusModifier) {
				var x = 0,
					y = 0,
					r = parent.teamWidth,
					c = 0,
					countPoints = data.length,
					countDotsInCircle = 0,
					currentIndex,
					tempVal,
					randomIndex,
					overflowFlag = 0,
					countCircles = [],
					offsetRadius = parent.userDotRadius * radiusModifier,
					step = 0;

				points = [];
				
				r -= offsetRadius;
				
				while (countPoints > 0 && overflowFlag < 1000) {
					if (c == 0) {
						r += offsetRadius;
						c = countDotsInCircle = Math.floor(Math.PI * 2 / (Math.asin((parent.userDotRadius * radiusModifier) / (2 * r)) * 2));
						t = 0;
					}
					x = Math.cos(Math.PI * 2 / countDotsInCircle * t) * r + parent.centerX;
					y = Math.sin(Math.PI * 2 / countDotsInCircle * t) * r + parent.centerY;
					
					if (y < parent.userDotRadius * 2 || y > parent.docHeight - parent.userDotRadius * 2 || x < parent.userDotRadius * 2 || x > parent.docWidth - parent.userDotRadius * 2) {
						overflowFlag++;
						c--;
						t++;
						continue;
					}
					//var needPoint = true;
					//while (needPoint) {
					//	x = Math.floor(Math.random() * (parent.docWidth - 20) + 10);
					//	y = Math.floor(Math.random() * (parent.docHeight - 20) + 10);
						 
					//	needPoint = CheckExistPoint(x, y);
					//}
					points.push([x, y]);
					c--;
					t++;
					countPoints--;
				}
				
				if (overflowFlag == 0)
					MakeRandomPoint(++radiusModifier);
				
				if (points.length !== data.length && overflowFlag > 998) {
					radiusModifier -= 0.5;
					MakeRandomPoint(radiusModifier);
				}

				currentIndex = points.length;
				while (0 !== currentIndex) {
					randomIndex = Math.floor(Math.random() * currentIndex);
					currentIndex--;
					tempVal = points[currentIndex];
					points[currentIndex] = points[randomIndex];
					points[randomIndex] = tempVal;
				}
			}
			
		/*	function CheckExistPoint(x, y) {
				var found = false;
				for (var i = 0; i < points.length; i++) {
					if (Math.abs(points[i][0] - x) < pointScale * 2 && Math.abs(points[i][1] - y) < pointScale * 2)
						found = true;
					if (Math.abs(x - parent.centerX) < teamWidth && Math.abs(y - parent.centerY) < teamWidth)
						found = true;
				}
				return found;
			}
			*/
			this.zoomed = !this.zoomed;
		}
	}
	company.InitWindow();
	company.DrawCompany();
	
	
})(d3);