<?
$a_departments = "Accounting department, EDP team, Export team, Administrative accounting, Development team, Management, Import department, IT department, Customer service, Logistics, Marketing, Sales department, Secretary's office, Public relations team";
$a_firstnames = "Adam, Antonio, Valentina, George, Paul, Andrea, Ben, Mike, Angela, Monika, Kelly, Sandra, Pamela, Robert, Sam, Lois, Michael";
$a_sournames = "Simon, Sandler, Ronario, Brown, McLain, De Mateo, Stiller, Mikelson, Petrelly, Belucci, Montana, Eastwood, Jackson, Anderson, Douglas, Simon, Assa, Obama";

$ans = array(
	"title" => "Company",
	"departments" => array()		
);
$departments = explode(", ", $a_departments);
$firstnames = explode(", ", $a_firstnames);
$sournames = explode(", ", $a_sournames);
shuffle($departments);
$f = sizeof($firstnames);
$s = sizeof($sournames);
$d = sizeof($departments);

for ($j = 0, $m = mt_rand(10, $d - 1); $j < $m; $j++) {
	$team = array();
	for ($i = 0, $c = mt_rand(100, 150); $i < $c; $i++) {
		$team[] = array(
			"name" => $firstnames[mt_rand(0, $f - 1)]." ".$sournames[mt_rand(0, $s - 1)],
			"phone" => "+1 ".mt_rand(100000000, 999999999)
		);
	}
	$ans["departments"][] = array(
		"title" => $departments[$j],
		"team" => $team
	);
}
print json_encode($ans);
?>